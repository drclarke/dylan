*
***
*******
***********
***************
*******************
***********************
* The Tenancy Project *
***********************
*******************
***************
***********
*******
***
*

** TO DO

* new coefficient plot formatting (in Atom)
* Unit Random Effects (RE) (Borusyak and Jaraval, 2018) regressions

clear all

global user "/Users/dylanclarke/Dropbox/Tenancy Project/current"
set more off

* set up switches
local rta=1 // switch on for Ontario to be treated by Residential Tenancy Act in 2006, off for Tenant Protection Act in 1996
local fullsample=1 // switch on to keep full sample (N=170), off to keep only provinces that are treated within the time series (N=150)
local moulton=0 // run Moulton factor calculations for intra-class correlation
local sumstat=0 // run summary statistics
local master=1 // creates the Master table (all 3 key dep. vars using unweighted OLS)
local repair=1 // run Prop 1 (repair) regressions
  local age=1 // run Prop 1 split by age
local homeownership=1 // run Prop 3 (homeownership) regressions
local rents=1 // run Prop 2 (rents) regressions
local afford=0 // run affordability regressions
local vacancy=0 // run vacancy rate regressions
local lags=1 // run lag/lead regressions and graphics
local trends=0 // create parallel trend graphical tests
local density=0 // create kernel density graphics (note: RTA must be switched on)


**********************
* 1. Create the data *
**********************

{

use "$user/data/housing1.dta"
append using "$user/data/2016census.dta"

merge m:1 cma year using "$user/data/chn1age.dta", nogen
merge m:1 cma year using "$user/data/rent.dta", nogen
merge m:1 cma year using "$user/data/vacrate.dta", nogen
merge m:1 cma year using "$user/data/hship.dta", nogen
replace hship = hship2016 if year==2016
drop hship2016
merge m:1 cma year using "$user/data/hstarts.dta", nogen
merge m:1 cma year using "$user/data/absorption.dta", nogen
merge m:1 cma using "$user/data/prov.dta", nogen

drop if cma=="Ottawa - Gatineau" || cma=="Belleville" || cma=="Lethbridge"


** temporary fixes for omitting 2016 obs
foreach var in chn1 chn1_h twobr_price hship {
  replace `var'=. if year==2016
}

* apply main treatment
gen law = 0
gen lawyear = 0
replace lawyear = 1975 if pr=="NB"
local nblaw = 1975
replace lawyear = 1989 if pr=="NS"
local nslaw = 1989
replace lawyear = 1990 if pr=="MB"
local mblaw = 1990
replace lawyear = 2000 if pr=="NF"
local nflaw = 2000
replace lawyear = 2002 if pr=="BC"
local bclaw = 2002
replace lawyear = 2006 if pr=="AB"
local ablaw = 2006
replace lawyear = 2006 if pr=="SK"
local sklaw = 2006
replace lawyear = 2010 if pr=="QC"
local qclaw = 2010

if `rta'==0 {
replace lawyear = 1996 if pr=="ON"
replace law = 1 if year>lawyear
}

else {
replace lawyear = 2006 if pr=="ON"
replace law = 1 if year>=lawyear
}
local onlaw = 1996 + 10*`rta'
*

* covariates
ren twobr_price rent
gen lrent = ln(rent)
gen lsc = ln(sc)
gen lsc_chn1 = ln(sc_chn1)
gen lsc_y = ln(sc_y)
gen lsc_m = ln(sc_m)
gen lsc_o = ln(sc_o)
gen lhstart = ln(hstart)
* gen lpermit = ln(permits)


* share of population renters
gen pct_ry = rpop_y/pop
gen pct_rm = rpop_m/pop
gen pct_ro = rpop_o/pop

gen hship1 = pop_h/cmapop

* demographics
gen pct_s = pop_s/cmapop
gen pct_o = pop_o/cmapop
gen pct_m = pop_m/cmapop
gen pct_y = pop_y/cmapop


gen linc = ln(avginc)
gen linc_h = ln(avginc_h)
gen linc_s = ln(avginc_s)

qui tab year, gen (_y)
qui tab pr, gen (_pr)
qui tab cma, gen (_cma)

gen chn1_rh = chn1 - chn1_h

sort cma year

* generate weights - 2001 CMA renter population
by cma: gen pop2001 = pop if year==2001
by cma: egen temp = max(pop2001)
replace pop2001 = temp if pop2001==.
drop temp

* generate weights - 2001 CMA population
sort cma year
by cma: gen cmapop2001 = cmapop if year==2001
by cma: egen temp = max(cmapop2001)
replace cmapop2001 = temp if cmapop2001==.
drop temp

* law-year fixed effects
foreach y in 1 2 3 4 5 {
gen law_year`y' = law*_y`y'
}
* law-province interactions
foreach p in 1 2 3 4 5 6 7 8 9 {
gen law_prov`p' = law*_pr`p'
}
* province-specific trends
foreach p in 1 2 3 4 5 6 7 8 9 {
gen prt`p' = _pr`p'*year
}
* province-year fixed effects
foreach y in 1 2 3 4 5 {
foreach p in 1 2 3 4 5 6 7 8 9 {
gen provyear`y'`p' = _pr`p'*_y`y'
}
}


save "$user/tenancy-final.dta", replace

}
*

if `fullsample'==0 {
keep if pr=="ON" || pr=="QC" || pr=="SK" || pr=="AB" || pr=="BC" || pr=="NF"
}
*


********************************************************************************
* 2. Compute Moulton Factor for repairs (P1) rents (P2) and homeownership (P3) *
********************************************************************************

if `moulton'==1 {
egen N_cma = count(cma), by (pr year)
sum N_cma
gen nbar = r(mean)
gen nvar = r(Var)
* set up the moulton factor for each var
foreach y in chn1 lrent hship {
loneway `y' pr
gen rho_`y' = r(rho)
gen mf_`y' = sqrt( 1 + (nvar/nbar + nbar - 1)*rho_`y')
}
sum mf*
}
*


*************************
* 3. Summary Statistics *
*************************

if `sumstat'==1 {

capture latabstat chn chn1 chn2 rent vacrate avginc hship hstart, ///
s(mean sd min p25 p50 p75 max n) col(statistics) format(%9.1f) tf("$user/results/sumstat/sumstat")replace

}
*


******************
* 4. Regressions *
******************


**********
* Master *
**********

if `master'==1 {
* repairs
qui reg chn1 law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Unweighted OLS. Standard errors clustered by province. Covariates are log of average CMA income, CMA vacancy rate, and log of CMA housing starts by year.")

qui reg chn1 law linc lrent vacrate lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

qui reg chn1 law linc lrent vacrate lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))

* rent prices
qui reg lrent law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents) ///

qui reg lrent law linc vacrate lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents)

qui reg lrent law linc vacrate lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% Chg. in Avg 2br Rents)

* homeownership
qui reg hship law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law chn1) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Homeownership Rates) ///

qui reg hship law linc vacrate lrent lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law chn1 linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Homeownership Rates)

qui reg hship law linc vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/aux/master.xls", append se tex ///
keep(law chn1 linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% Chg. in Homeownership Rates)

}
*

**********
* Repair *
**********

if `repair'==1 {

* OLS clustered by province

qui reg chn1 law _cma* _y*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p1 = `r(p)'
outreg2 using "$user/results/repair/repair.xls", replace se tex ///
addstat(Wild bootstrap p-value, `p1') ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(Share of Core Housing Need (%pt)) ///
addnote("Unweighted OLS. Standard errors clustered by province. Wild bootstrap \$p\$-values calculated with Webb (2014) weights. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn1 law linc lrent vacrate lhstart _cma* _y*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p2 = `r(p)'
outreg2 using "$user/results/repair/repair.xls", append se tex ///
addstat(Wild bootstrap p-value, `p2') ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(Share of Core Housing Need (%pt))

qui reg chn1 law linc lrent vacrate lhstart _cma* _y* prt*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p3 = `r(p)'
outreg2 using "$user/results/repair/repair.xls", append se tex ///
addstat(Wild bootstrap p-value, `p3') ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ///
ctitle(Share of Core Housing Need (%pt))

* WLS clustered by province

qui reg chn1 law _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/repair/repair_weighted.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Weighted Least Squares. Observations weighted by 2001 CMA renter population. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn1 law linc lrent vacrate lhstart _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/repair/repair_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

qui reg chn1 law linc lrent vacrate lhstart _cma* _y* prt* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/repair/repair_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))

* Placebo Test: Homeowners
// calculate f-stat for treatment-placebo coefficient comparison
qui reg chn1 law _cma* _y*
  est store rent1
qui reg chn1_h law _cma* _y*
  est store home1

qui reg chn1 law linc lrent vacrate lhstart _cma* _y*
  est store rent2
qui reg chn1_h law linc_h vacrate lrent lhstart _cma* _y*
  est store home2

qui reg chn1 law linc lrent vacrate lhstart _cma* _y* prt*
  est store rent3
qui reg chn1_h law linc_h vacrate lrent lhstart _cma* _y* prt*
  est store home3

qui suest rent1 home1 rent2 home2 rent3 home3, cl(pr)

forvalues r = 1(1)3 {
  test [rent`r'_mean]law = [home`r'_mean]law
  local chi`r' = `r(chi2)'
}

test [rent1_mean]law=[home1_mean]law
local f1 = `r(chi2)'

test [rent2_mean]law=[home2_mean]law
local f2 = `r(chi2)'

test [rent3_mean]law=[home3_mean]law
local f3 = `r(chi2)'

// homeowner population regressions
qui reg chn1_h law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/repair/repairplacebo.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(Share of Core Housing Need (%pt)) ///
addstat($\chi ^2$, `f1') ///
addnote("Unweighted OLS. Placebo group is homeowner population. $\chi ^2$ statistic is result of test between reported homeowner population regression coefficient and the corresponding renter population regression coefficient. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn1_h law linc_h vacrate lrent lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/repair/repairplacebo.xls", append se tex ///
keep(law linc_h lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(Share of Core Housing Need (%pt)) ///
addstat($\chi ^2$, `f2')

qui reg chn1_h law linc_h vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/repair/repairplacebo.xls", append se tex ///
keep(law linc_h lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ///
ctitle(Share of Core Housing Need (%pt)) ///
addstat($\chi ^2$, `f3')

* Wild Bootstrap

/*
cgmwildboot chn1 law _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/repair/repairwildbs.xls", replace se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Wild cluster bootstrap $p$-values clustered by province (400 reps). Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

cgmwildboot chn1 law linc lrent vacrate lhstart _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/repair/repairwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

cgmwildboot chn1 law linc lrent vacrate lhstart _cma* _y* prt*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/repair/repairwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))
*/

/*
*	*	*	*	*	*	*	*
* Heterogeneous Effects: Age
*	*	*	*	*	*	*	*

* 15-29 Years Old

qui reg chn1_y law _cma* _y*, cl(pr)
outreg2 using "$user/results/repair/repairage.xls", nocons replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(15-29 Years) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn1_y law linc vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/repair/repairage.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(15-29 Years)

* 30-44 Years Old

qui reg chn1_m law _cma* _y*, cl(pr)
outreg2 using "$user/results/repair/repairage.xls", nocons append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(30-44 Years)

qui reg chn1_m law linc vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/repair/repairage.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(30-44 Years)

* 45-65 Years Old

qui reg chn1_o law _cma* _y*, cl(pr)
outreg2 using "$user/results/repair/repairage.xls", nocons append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(45-64 Years)

qui reg chn1_o law linc vacrate lrent lhstart _cma* _y* prt*, cl(pr)
outreg2 using "$user/results/repair/repairage.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(45-64 Years)

* Seniors

qui reg chn1_s law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/repair/repairage.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(65+ Years)

qui reg chn1_s law linc vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/repair/repairage.xls", append se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(65+ Years)
*/

}
*

*****************
* Homeownership *
*****************

if `homeownership'==1 {

qui reg hship law _cma* _y*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p1 = `r(p)'
outreg2 using "$user/results/exit/hship.xls", replace se tex ///
addstat(Wild bootstrap p-value, `p1') ///
keep(law chn1) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(% Chg. in Homeownership Rates) ///
addnote("Unweighted OLS. Standard errors clustered by province. wild bootstra \$p\$-values calculated using Webb (2014) weights. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg hship law linc vacrate lrent lhstart _cma* _y*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p2 = `r(p)'
outreg2 using "$user/results/exit/hship.xls", append se tex ///
addstat(Wild bootstrap p-value, `p2') ///
keep(law chn1 linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ///
ctitle(% Chg. in Homeownership Rates)

qui reg hship law linc vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
boottest law, boottype(wild) weight(webb) nogr
local p3 = `r(p)'
outreg2 using "$user/results/exit/hship.xls", append se tex ///
addstat(Wild bootstrap p-value, `p3') ///
keep(law chn1 linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ///
ctitle(% Chg. in Homeownership Rates)

* Wild Bootstrap

/*
cgmwildboot hship law _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/exit/hshipwildbs.xls", replace se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(% Chg. in Homeownership Rates) ///
addnote("Wild cluster bootstrap $p$-values clustered by province (400 reps). Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

cgmwildboot hship law linc vacrate lhstart _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/exit/hshipwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, .) ctitle(% Chg. in Homeownership Rates)

cgmwildboot hship law linc vacrate lrent lhstart _cma* _y* prt*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/exit/hshipwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(% Chg. in Homeownership Rates)
*/

** WLS clustered by province **

qui reg hship law _cma* _y* [aw=cmapop2001], nocons cl(pr)
outreg2 using "$user/results/exit/hship_weighted.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Homeownership Rates) ///
addnote("Weighted Least Squares. Observations weighted by 2001 CMA total population. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg hship law linc vacrate lrent lhstart _cma* _y* [aw=cmapop2001], nocons cl(pr)
outreg2 using "$user/results/exit/hship_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Homeownership Rates)

qui reg hship law linc vacrate lrent lhstart _cma* _y* prt* [aw=cmapop2001], nocons cl(pr)
outreg2 using "$user/results/exit/hship_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% Chg. in Homeownership Rates)

}
*

*********************************
* Average 2 Bedroom Rent Prices *
*********************************

if `rents'==1 {

qui reg lrent law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/rent/rentprices.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, and log of CMA housing starts by year.")

qui reg lrent law linc vacrate lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/rent/rentprices.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents)

qui reg lrent law linc vacrate lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/rent/rentprices.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% Chg. in Avg 2br Rents)

* Weighted

qui reg lrent law _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/rent/rentprices_weighted.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents) ///
addnote("Weighted Least Squares. Observations weighted by 2001 CMA renter population. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, and log of CMA housing starts by year.")

qui reg lrent law linc vacrate lhstart _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/rent/rentprices_weighted.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% Chg. in Avg 2br Rents)

qui reg lrent law linc vacrate lhstart _cma* _y* prt* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/rent/rentprices_weighted.xls", append se tex ///
keep(law linc vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% Chg. in Avg 2br Rents)

}
*

*****************
* Affordability *
*****************

if `afford'==1 {

* OLS clustered by province

qui reg chn2 law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/afford/afford.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn2 law linc lrent vacrate lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/afford/afford.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

qui reg chn2 law linc lrent vacrate lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/afford/afford.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))

* WLS clustered by province

qui reg chn2 law _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/afford/afford_weighted.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Weighted Least Squares. Observations weighted by 2001 CMA renter population. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn2 law linc lrent vacrate lhstart _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/afford/afford_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

qui reg chn2 law linc lrent vacrate lhstart _cma* _y* prt* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/afford/afford_weighted.xls", append se tex ///
keep(law linc lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))

* Placebo Test: Homeowners

qui reg chn2_h law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/afford/affordplacebo.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Unweighted OLS. Placebo group is homeowner population. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg chn2_h law linc_h vacrate lrent lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/afford/affordplacebo.xls", append se tex ///
keep(law linc_h lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

qui reg chn2_h law linc_h vacrate lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/afford/affordplacebo.xls", append se tex ///
keep(law linc_h lrent vacrate lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))

* Wild Bootstrap

cgmwildboot chn2 law _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/afford/affordwildbs.xls", replace se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, ., Prov Trends?, .) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Wild cluster bootstrap $p$-values clustered by province (400 reps). Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

cgmwildboot chn2 law linc lrent vacrate lhstart _cma* _y*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/afford/affordwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, .) ctitle(Share of Core Housing Need (%pt))

cgmwildboot chn2 law linc lrent vacrate lhstart _cma* _y* prt*, cluster(pr) bootcluster(pr) reps(400)
outreg2 using "$user/results/afford/affordwildbs.xls", append se tex stats(coef pval) pdec(3) ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Covariates?, X, Prov Trends?, X) ctitle(Share of Core Housing Need (%pt))


}
*

*****************
* Vacancy Rates *
*****************

if `vacancy'==1 {

qui reg vacrate law _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% pt. chg. in Vacancy Rates) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg vacrate law linc hship lrent lhstart _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates.xls", append se tex ///
keep(law linc hship lrent lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% pt. chg. in Vacancy Rates)

qui reg vacrate law linc hship lrent lhstart _cma* _y* prt*, nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates.xls", append se tex ///
keep(law linc hship lrent lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% pt. chg. in Vacancy Rates)


* Weighted

qui reg vacrate law _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates_weighted.xls", replace se tex ///
keep(law) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% pt. chg. in Vacancy Rates) ///
addnote("Weighted Least Squares. Observations weighted by 2001 CMA renter population. Standard errors clustered by province. Full set of covariates are log of average CMA income, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year.")

qui reg vacrate law linc hship lrent lhstart _cma* _y* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates_weighted.xls", append se tex ///
keep(law linc hship lrent hship lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, .) ctitle(% pt. chg. in Vacancy Rates)

qui reg vacrate law linc hship lrent lhstart _cma* _y* prt* [aw=pop2001], nocons cl(pr)
outreg2 using "$user/results/vacancy/vacancyrates_weighted.xls", append se tex ///
keep(law linc hship lrent lhstart) addtext(CMA FE?, X, Year FE?, X, Prov Trends?, X) ctitle(% pt. chg. in Vacancy Rates)

}
*


*************
* 5. Graphs *
*************

***********************
* Lag/Lead Regression *
***********************

if `lags'==1 {
* create the dataset

{

sort cma year

* set panel for Borusyak and Jaravel (2018) robustness check
egen city = group(cma)
xtset city year

* re-number each year
bys cma: gen t = 1 if _n==1
foreach y in 2 3 4 5 6 {
bys cma: replace t = `y' if _n==`y'
}
* number the letters
gen y1 = t if law==1

* obtain the differencing number
egen y2 = min(y1), by(pr)

* difference away to get event space
gen y = t-y2

* before and after dummies
qui tab y, gen (_ty)

drop y y1 y2

* instrument for Shapiro et al. (2018) robustness check
bys city: gen law_lead = law[_n+1]

* before and after interactions
foreach y in 1 2 3 4 5 6 7 8 9 {
gen ylaw`y' = law*_ty`y'
}
*
foreach y in 1 2 3 4 5 6 7 8 9 {
foreach p in 1 2 3 4 5 6 7 8 9 {
gen py`y'`p' = _pr`p'*_ty`y'
}
}

}
*

* event study figures

{

qui eststo repair_r: reg chn1 _cma* _y* _ty*, cl(pr)
qui eststo repair_rc: reg chn1 vacrate lrent lhstart _cma* _y* _ty*, cl(pr)
qui eststo repair_h: reg chn1_h _cma* _y* _ty*, cl(pr)
qui eststo repair_hc: reg chn1_h lrent vacrate lhstart _cma* _y* _ty*, cl(pr)

coefplot (repair_rc, lab("Renters")offset(0.05)msym(S)mcol(black)mfcol(black)msize(small)ciopts(recast(rcap)lpatt(solid)lcol(black))) ///
(repair_hc, lab("Homeowners")offset(-0.05)msym(D)mcol(gray)mfcol(white)msize(small)ciopts(recast(rcap)lpatt(dash)lcol(gray))), ///
keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair.pdf", as(pdf) replace

coefplot (repair_r, lab("No Controls")offset(-0.05)msym(D)msize(small)mcol(blue)mfcol(white)ciopts(recast(rcap))) ///
(repair_rc, lab("Controls")offset(0.05)msym(S)msize(small)mcol(red)mfcol(red)ciopts(recast(rcap))), ///
keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair_controls.pdf", as(pdf) replace

qui eststo rent: reg lrent _cma* _y* _ty*, cl(pr)
qui eststo rent_c: reg lrent vacrate lhstart _cma* _y* _ty*, cl(pr)

coefplot (rent, lab("No Controls")offset(-0.05)msym(D)msize(small)mcol(blue)mfcol(white)ciopts(recast(rcap)lpatt(dash)lcol(blue))) ///
(rent_c, lab("Controls")offset(0.05)msym(S)msize(small)mcol(red)mfcol(red)ciopts(recast(rcap)lpatt(solid)lcol(red))), ///
keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% chg. in avg. rent prices) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/rent_controls.pdf", as(pdf) replace

qui eststo hship: reg hship _cma* _y* _ty*, cl(pr)
qui eststo hship_c: reg hship lhstart vacrate lrent _cma* _y* _ty*, cl(pr)

coefplot (hship, lab("No Controls")offset(-0.05)msym(D)msize(small)mcol(blue)mfcol(white)ciopts(recast(rcap)lpatt(dash)lcol(blue))) ///
(hship_c, lab("Controls")offset(0.05)msym(S)msize(small)mcol(red)mfcol(red)ciopts(recast(rcap)lpatt(solid)lcol(red))), ///
keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in homeownership) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/hship_controls.pdf", as(pdf) replace

qui eststo vacrate: reg vacrate _cma* _y* _ty*, cl(pr)
qui eststo vacrate_c: reg vacrate lrent lhstart _cma* _y* _ty*, cl(pr)

coefplot (vacrate, lab("No Controls")offset(-0.05)msym(D)msize(small)mcol(blue)mfcol(white)ciopts(recast(rcap)lpatt(dash)lcol(blue))) ///
(vacrate_c, lab("Controls")offset(0.05)msym(S)msize(small)mcol(red)mfcol(red)ciopts(recast(rcap)lpatt(solid)lcol(red))), ///
keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in vacancy rates) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/vacrate_controls.pdf", as(pdf) replace


}
*

* leading and lagged treatment regressions

{
qui reg chn1 _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/repairleads.xls", replace se tex ///
keep(_ty*) addtext(Covariates?, ., CMA FE?, X, Year FE?, X) ctitle(Share of Core Housing Need (%pt)) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA 2 bedroom rent prices, and log of CMA housing starts by year. Coefficient estimates reflect law and lead/lag interactions.")

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair1.pdf", as(pdf) replace

qui reg chn1 vacrate lrent lhstart _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/repairleads.xls", append se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(Share of Core Housing Need (%pt))

coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair2.pdf", as(pdf) replace

* placebo population: homeowners
qui reg chn1_h _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/repairleads.xls", append se tex ///
keep(_ty*) addtext(Covariates?, ., CMA FE?, X, Year FE?, X) ctitle(Placebo)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair3.pdf", as(pdf) replace


qui reg chn1_h lrent vacrate lhstart _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/repairleads.xls", append se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(Placebo)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repairs) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair4.pdf", as(pdf) replace

* rent prices
qui reg lrent _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/rentsleads.xls", replace se tex ///
keep(_ty*) addtext(Covariates?, ., CMA FE?, X, Year FE?, X) ctitle(Rent Prices) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, and log of CMA housing starts by year. Coefficient estimates reflect law and lead/lag interactions.")

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% chg. in rent prices) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/rents1.pdf", as(pdf) replace

qui reg lrent vacrate lhstart _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/rentsleads.xls", append se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(Rent Prices)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% chg. in rent prices ) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/rents2.pdf", as(pdf) replace

* homeownership
qui reg hship _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/hshipleads.xls", replace se tex ///
keep(_ty*) addtext(Covariates?, ., CMA FE?, X, Year FE?, X) ctitle(Homeownership) ///
addnote("Unweighted OLS. Standard errors clustered by province. Full set of covariates are log of average CMA income, CMA vacancy rate, log of average CMA two bedroom rent prices, and log of CMA housing starts by year. Coefficient estimates reflect law and lead/lag interactions.")

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in homeownership) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/hship1.pdf", as(pdf) replace

qui reg hship lhstart vacrate lrent _cma* _y* _ty*, nocons cl(pr)
outreg2 using "$user/results/granger/hshipleads.xls", append se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(Homeownership)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in homeownership) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/hship2.pdf", as(pdf) replace

}
*

* Pretrends IV Shapiro et al. (2018) regressions

{

ivreg2 chn1 (vacrate=law_lead) _ty2 _ty3 _ty5 _ty6 _ty7 _ty8 _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/granger/repair_FHS_iv.xls", replace se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(FHS IV, Vacancy Rate)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repair) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair_FHS_vacrateiv.pdf", as(pdf) replace


ivreg2 chn1 (hship=law_lead) _ty2 _ty3 _ty5 _ty6 _ty7 _ty8 _cma* _y*, nocons cl(pr)
outreg2 using "$user/results/granger/repair_FHS_iv.xls", append se tex ///
keep(_ty*) addtext(Covariates?, X, CMA FE?, X, Year FE?, X) ctitle(FHS IV, Homeownership)

qui coefplot, keep(_ty*) yline(0, lcol(black)lpatt(dash)) vertical ytitle(% pt. chg. in repair) xtitle(Law Year) nolabel ///
coeflabels(_ty2="t-15" _ty3="t-10" _ty4="t-5" _ty5="t" _ty6="t+5" _ty7="t+10" _ty8="t+15") ///
msize(small)msym(S)mcol(black)ciopts(recast(rcap)lcol(black)lpatt(solid)) ///
graphregion(color(white)) bgcolor(white)
graph export "$user/graphics/betaplot/repair_FHS_hshipiv.pdf", as(pdf) replace

}
*

}
*

********************
* Parallel Trends  *
********************

if `trends'==1 {

cd "$user/graphics/pretrends"

** Scatter Plot **
preserve

collapse (mean) chn1 rent hship vacrate lawyear law [aw=pop], by(year pr)

local nblaw = 1975
local nslaw = 1989
local mblaw = 1990
local nflaw = 2000
local bclaw = 2002
local ablaw = 2006
local sklaw = 2006
local qclaw = 2010
local onlaw = 1996 + 10*`rta'

tw scatter chn1 year if pr=="NF", ti("Newfoundland", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`nflaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(NF, replace)
tw scatter chn1 year if pr=="BC", ti("British Columbia", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`bclaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(BC, replace)
tw scatter chn1 year if pr=="ON", ti("Ontario", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`onlaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(ON, replace)
tw scatter chn1 year if pr=="AB", ti("Alberta", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`ablaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(AB, replace)
tw scatter chn1 year if pr=="SK", ti("Saskatchewan", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`sklaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(SK, replace)
tw scatter chn1 year if pr=="QC", ti("Quebec", col(black)) xti("Year", col(black)) yti("% w/ major repair", col(black)) mcol(black)mfcol(white) xline(`qclaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(QC, replace)
gr combine BC.gph AB.gph SK.gph ON.gph QC.gph NF.gph, ycommon graphregion(color(white)) saving(pretrends, replace)


foreach y in rent hship vacrate {

tw scatter `y' year if pr=="NF", ti("Newfoundland", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`nflaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(NF_`y', replace)
tw scatter `y' year if pr=="BC", ti("British Columbia", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`bclaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(BC_`y', replace)
tw scatter `y' year if pr=="ON", ti("Ontario", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`onlaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(ON_`y', replace)
tw scatter `y' year if pr=="AB", ti("Alberta", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`ablaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(AB_`y', replace)
tw scatter `y' year if pr=="SK", ti("Saskatchewan", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`sklaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(SK_`y', replace)
tw scatter `y' year if pr=="QC", ti("Quebec", col(black)) xti("Year", col(black)) yti("`y'", col(black)) mcol(black)mfcol(white) xline(`qclaw', lpatt(dash)lcol(black)) graphregion(color(white)) bgcolor(white) saving(QC_`y', replace)
gr combine BC_`y'.gph AB_`y'.gph SK_`y'.gph ON_`y'.gph QC_`y'.gph NF_`y'.gph, ycommon graphregion(color(white)) saving(pretrends_`y', replace)

}
restore

}
*

******************
* Kernel Density *
******************

if `rta'==1 & `density'==1 {

cd "$user/graphics/density plots"

** Repairs

su chn1 if law==0, meanonly
kdensity chn1 if law==0, ti("Renters before the law", col(black)) xli(`r(mean)', lcol(black)lpatt(dash)) xti("Share of renters in repair (% pt.)") col(black) saving(renter_pre, replace)

su chn1 if law==1, meanonly
kdensity chn1 if law==1, ti("Renters after the law", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share of renters in repair (% pt.)") col(black) saving(renter_post, replace)

su chn1_h if law==0, meanonly
kdensity chn1_h if law==0, ti("Homeowners before the law", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share of homeowners in repair (% pt.)") col(black) saving(homeowner_pre, replace)

su chn1_h if law==1, meanonly
kdensity chn1_h if law==1, ti("Homeowners after the law", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share of homeowners in repair (% pt.)") col(black) saving(homeowner_post, replace)

gr combine renter_pre.gph renter_post.gph homeowner_pre.gph homeowner_post.gph, ycommon xcommon saving(kdensity_repairs, replace)

*

* Repairs: 2001, 2006, 2011

preserve

keep if pr=="ON" || pr=="SK" || pr=="AB" || pr=="BC"

su chn1 if year==2001, meanonly
kdensity chn1 if year==2001, bwidth(0.55) ti("Renter-occupied: 2001", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(renter_2001, replace)

su chn1 if year==2006, meanonly
kdensity chn1 if year==2006, bwidth(0.55) ti("Renter-occupied: 2006", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(renter_2006, replace)

su chn1 if year==2011, meanonly
kdensity chn1 if year==2011, bwidth(0.55) ti("Renter-occupied: 2011", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(renter_2011, replace)

su chn1_h if year==2001, meanonly
kdensity chn1_h if year==2001, bwidth(0.55) ti("Owner-occupied: 2001", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(homeowner_2001, replace)

su chn1_h if year==2006, meanonly
kdensity chn1_h if year==2006, bwidth(0.55) ti("Owner-occupied: 2006", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(homeowner_2006, replace)

su chn1_h if year==2011, meanonly
kdensity chn1_h if year==2011, bwidth(0.55) ti("Owner-occupied: 2011", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Share with major repair (% pt.)") col(black) graphregion(color(white)) bgcolor(white) saving(homeowner_2011, replace)


gr combine renter_2001.gph renter_2006.gph renter_2011.gph homeowner_2001.gph homeowner_2006.gph homeowner_2011.gph, ycommon xcommon graphregion(color(white)) saving(kdensity_3t_repairs, replace)
* note: Observations are CMAs in provinces treated between 2001 and 2006 (AB, BC, ON, SK)

restore

** Homeownership

su hship if year==2001, meanonly
kdensity hship if year==2001, ti("Homeownership (2001)", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Homeownership rate (% pt.)") lcolor(black) graphregion(color(white)) bgcolor(white) saving(hship_2001, replace)

su hship if year==2006, meanonly
kdensity hship if year==2006, ti("Homeownership (2006)", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Homeownership rate (% pt.)") lcolor(black) graphregion(color(white)) bgcolor(white) saving(hship_2006, replace)

su hship if year==2011, meanonly
kdensity hship if year==2011, ti("Homeownership (2011)", col(black)) xli(`r(mean)', lcol(black)lpa(dash)) xti("Homeownership rate (% pt.)") lcolor(black) graphregion(color(white)) bgcolor(white) saving(hship_2011, replace)

gr combine hship_2001.gph hship_2006.gph hship_2011.gph, ycommon xcommon graphregion(color(white)) saving(kdensity_3t_hship, replace)

gr combine renter_2001.gph renter_2006.gph renter_2011.gph homeowner_2001.gph homeowner_2006.gph homeowner_2011.gph hship_2001.gph hship_2006.gph hship_2011.gph, graphregion(color(white)) saving(kdensity_3t, replace)
}
*
